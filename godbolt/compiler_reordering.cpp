int a;
int b;

// x86-64, gcc 10.1, -O2 => reordered writes
void ReorderMe() {
  a = b + 1;  // store to `a` goes first
  b = 1;
}

int main() {
  return 0;
}
