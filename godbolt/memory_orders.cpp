#include <atomic>

std::atomic<int> test;

void StoreSeqCst() {
  test.store(52, std::memory_order::seq_cst);
}

void StoreRelease() {
  test.store(47, std::memory_order::release);
}

void StoreRelaxed() {
  test.store(17, std::memory_order::relaxed);
}

// Use -O2 -std=c++20

int main() {
  return 0;
}
