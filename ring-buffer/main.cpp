#include "ring_buffer.hpp"

#include <thread>

int main() {
  SPSCRingBuffer<int> buf(256);

  static const int kValues = 100500;

  // Single producer
  std::thread producer([&]() {
    for (int i = 0; i < kValues; ++i) {
      while (!buf.Publish(i)) {
        ;  // Retry
      }
    }
  });

  // Single consumer
  std::thread consumer([&]() {
    for (int i = 0; i < kValues; ++i) {
      int value;
      while (!buf.Consume(value)) {
        ;  // Retry
      }
    }
  });

  producer.join();
  consumer.join();

  return 0;
};
